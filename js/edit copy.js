 import {getCourses} from "./callbacks.js"; 

let container = document.querySelector('.tabla-cursos');
let allCourse=[];
async function renderCourses() {
    let cursos = await getCourses();
    let info=infoCourse(cursos);
 let tabla= document.createElement("div");
    let cajaModulos= document.createElement("div");
    let cajaUnidades= document.createElement("div");
    let cajaCurso= document.createElement("div");
    let tr= document.createElement("div");
    tr.setAttribute("class","dis-g-gtc-4 ali-i-c all-label-dis-b");      

        tr.innerHTML+=
        info.cursos+
        "<div>"+info.modulos+"</div>"+
        "<div>"+info.unidades+"</div>"+
        "<div>"+info.paginas+"</div>";

    
    tabla.appendChild(tr);
    container.innerHTML='<div class="dis-g-gtc-4 ali-i-c"><div>Cursos</div><div>Módulos</div><div>Unidades</div><div>Páginas</div></div>';
    container.appendChild(tabla); 
}

function infoCourse(cursos){
    
    cursos.forEach((curso,index) => {
        allCourse.curso=getCourse(index);                     
        curso["curso"+index].forEach((modulos,num) => {
            getModulos(index,modulos["modulos"+num]);
            /* modulos["modulos"+num].forEach((unidad,ind) => {   
                numCourse.unidades=getUnidades(index,modulos["modulos"+num],unidad);   
                numCourse.paginas=getPaginas(index,num,ind,unidad);    
            }); */
        }); 
        
    });
}
function getCourse(index){
    let curso=`
    <div>
    <label>
        Curso ${index}
    </label>
    <input data-i="curso${index}" name="titulo">
    </div>
    `;
    return curso;
}
function getModulos(curso,modulos){
    let tr=document.createElement("div");
    for (let index = 0; index < modulos.length; index++){    
        let modulo =`
            <label>Modulo ${index}</label>
            <input data-root="curso${curso}" data-i="modulo${index}" name="titulo">
        `;
        tr.innerHTML+="<div>"+modulo+"<div>";
    };
return tr.innerHTML;  
}
function getUnidades(curso,modulos,unidades){
     let tr=document.createElement("div");
    for (let index = 0; index < modulos.length; index++) {
        let itemBack =`
        <label>Unidad ${index}</label>
        <input data-root="curso${curso}" data-padre="modulo${index}" name="titulo">
    `;
        tr.innerHTML+=itemBack;
    }
return tr.innerHTML; 
}
function getPaginas(curso,modulos,unidades,paginas){
   /* let tr=document.createElement("div");
  
    let select=document.createElement("select");
    
    paginas.forEach((element,index) => {
       let opciones= getSelects(element.paginas);
       let itemBack=`
       <label>Paginas del modulo${index}</label><select>${opciones.innerHTML}</select>`;
       tr.innerHTML+=itemBack;
   }); 
   return tr; */
}




function getSelects(count){
     let select=document.createElement("select");    
    for (let index = 0; index < count; index++) {
        let options=document.createElement("option");
        options.innerHTML=index
        options.value=index
        select.appendChild(options)
    }
    return select; 
}

renderCourses(); 