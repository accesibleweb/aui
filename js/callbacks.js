export async function getUrls() {
    let url = 'http://localhost/fireCat/cursos/config.json';
    try {
        let res = await fetch(url);
        return await res.json();
    } catch (error) {
        console.log(error);
    }
}
export async function getCourses(urlCurso) {
    let url = `http://localhost/fireCat/cursos/${urlCurso}/config.json`;
    try {
        let res = await fetch(url);
        return await res.json();
    } catch (error) {
        console.log(error);
    }
}