 import {getUrls} from "./callbacks.js"; 
 import {getCourses} from "./callbacks.js";
 import {createInputLabelButton} from "./functions.js";
 import {createInputLabel} from "./functions.js";
 import {createSelectLabel} from "./functions.js";

const urls = getUrls();
urls.then(res => {
    res.forEach((element,index) => {
        pintaDivs(element[0]);
    });
}).catch(err => console.log(err));


 function pintaDivs(cantidad){
     let div= document.createElement("div");
     div.setAttribute("class","bor-y-1-c-gr");
     let fragment = document.createDocumentFragment();
     let form = document.createElement("form");
     form.setAttribute("class","all-div-pad-y-12p");
     form.setAttribute("id",cantidad);
     form.setAttribute("action","dataEdit.php");
     form.setAttribute("method","post");
     let btnSend= document.createElement("button");
     btnSend.setAttribute("type","button");
     btnSend.setAttribute("onclick",`dataSend('${cantidad}')`);
     btnSend.innerHTML="Editar "+cantidad;
     getCourses(cantidad).then(response => {
        for (const prop in response) { 
            let labelName= prop;
            switch(prop){
                case "numero":
                    form.innerHTML += createInputLabel("titulo curso","titulocurso","");
                break;
                case "menuPrincipal":
                    form.innerHTML += createSelectLabel(prop, prop, mainMenun());
                break;
                case "menuSecundario":
                    form.innerHTML += createSelectLabel(prop, prop, mainMenun());
                break;
                case "plantilla":
                    form.innerHTML += createSelectLabel(prop, prop, mainMenun());
                break;
                case "paginado":
                    form.innerHTML += createSelectLabel(prop, prop, mainMenun());
                break;
                case "modulos":
                    form.innerHTML +=  createInputLabelButton(
                        prop,
                        prop,
                        "",
                        "editar",
                        `onclick="creaComponentes(this,'${labelName}')"`,
                        ""
                      );
                break;
                case "unidades":
                    form.innerHTML +=  createInputLabelButton(
                        prop,
                        prop,
                        "",
                        "editar",
                        `onclick="creaComponentes(this,'${labelName}')"`,
                        ""
                      );
                break;

            }
        }
        form.insertBefore(btnSend, form.lastElementChild.nextSibling);
     });
     fragment.appendChild(form);
     div.appendChild(fragment);
     document.querySelector("#tablaCursos").appendChild(div);      
}

function mainMenun(){
    let options= `
    <option value="defecto" selected>defecto</option>
    <option value="Hamburquesa">Hamburquesa</option>
    <option value="Horizontal">Horizontal</option>
    <option value="Vertical">Vertical</option>
    <option value="HamburguesapopUp">HamburguesapopUp</option>
    `;
    return options;
}
