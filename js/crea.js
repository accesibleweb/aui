document.querySelector("#cursosBtn").addEventListener("click", (e) => {
  e.preventDefault();
  const data = new FormData();
  let cuantos = document.querySelector(`input[name="cuantos"]`).value;
  data.append("cuantos", cuantos);
  dataForm(data);
});

function dataForm(data) {
  fetch('http://localhost/fireCat-Git/aui/dataCreate.php', {
    method: 'POST',
    body: data
  })
    .then(response => {
      if (response.status === 200) {
        let cuantos = document.querySelector(`input[name="cuantos"]`).value;
        delNode("divCurso");
        for (let index = 0; index < cuantos; index++) {
          let form = createForm("", "curso" + index, "POST");
          form.setAttribute("class","bor-y-1-c-gr mar-y-12p pad-y-12p all-inp-dis-b");
          form.appendChild(createLabel("",`Del curso ${index} cuantos módulos?`));
          form.appendChild(createInput("","modulos",`curso${index}`));
          form.appendChild(button("enviar", "button", "", `moduleSend(this)`));
          insertNode(`creaCurso`, form);
        }
      }
    })
    .catch(function (err) {
      console.log(err);
    });
}
function moduleSend(este) {
  let nameForm = este.parentNode.id;
  let form= document.querySelector(`#${nameForm}`);
  let inptVal = este.parentNode.querySelector("input").value; 
  const data = new FormData();
  data.append("modulos", inptVal);
  data.append("formulario", nameForm);
  fetch('http://localhost/fireCat-Git/aui/dataCreate.php', {
    method: 'POST',
    body: data
  })
    .then(response => {
      if (response.status === 200) {
        document.querySelector(`#${nameForm}`).innerHTML="";
        for (let index = 0; index < inptVal; index++) {
          form.append(createLabel("",`Del ${nameForm} y del modulo${index}... cuantas unidades?`));
          form.append(createInput("","unidades",`${nameForm},modulo${index}`));
         
        }
        form.appendChild(button("enviar", "button", "", `unidadSend(this)`));
      }
    })
    .catch(function (err) {
      console.log(err);
    });
}
function unidadSend(este) {
  let inptCant = este.parentNode.querySelectorAll("input");
  let labelCant = este.parentNode.querySelectorAll("label");
  let form = document.querySelector(`#${este.parentNode.id}`);
  for (let index = 0; index < inptCant.length; index++) {
    const data = new FormData();
    let dataInfo= inptCant[index].getAttribute("data");
    dataInfo=dataInfo.split(",");
    let curso= dataInfo[0];
    let modulo= dataInfo[1];
    let unidad=inptCant[index].value;
    data.append("curso",curso);
    data.append("modulo",modulo);
    data.append("unidad",unidad);
    fetch('http://localhost/fireCat-Git/aui/dataCreate.php', {
      method: 'POST',
      body: data
    }).then(response =>{
      if (response.status === 200) {
        let valor=inptCant[index].value;
        inptCant[index].remove();
        labelCant[index].remove();
        for (let indexPage = 0; indexPage < valor; indexPage++) {
          form.append(createLabel("",`Del ${curso} el ${modulo} de la unidad${indexPage} cuantas páginas?`));
          form.appendChild(createInput("","paginas",`${curso},${modulo},unidad${index}`));          
        }
        form.querySelector("button").setAttribute("onclick","pageSend(this)");
      }
    });
  }
}
function pageSend(este) {
  let labelCant = este.parentNode.querySelectorAll("label");
  let inptCant = este.parentNode.querySelectorAll("input");
   for (let index = 0; index < inptCant.length; index++) {
    const data = new FormData();
    let dataInfo= inptCant[index].getAttribute("data");
    dataInfo=dataInfo.split(",");
    let curso= dataInfo[0];
    let modulo= dataInfo[1];
    let unidad=dataInfo[2];
    let paginas=inptCant[index].value;
    data.append("cursoPage",curso);
    data.append("moduloPage",modulo);
    data.append("unidadPage",unidad);
    data.append("paginas",paginas);
    fetch('http://localhost/fireCat-Git/aui/dataCreate.php', {
      method: 'POST',
      body: data
    }).then(response =>{
      if (response.status === 200) {
        inptCant[index].remove();
        labelCant[index].remove();
      }
    });
  } 
  let btn=este.parentNode.querySelector("button");
  btn.innerHTML="Editar";
  btn.setAttribute("onclick","location.href='edita-curso.html'");
}
function delNode(id) {
  document.querySelector(`#${id}`).remove();
}
function insertNode(id, que) {
  let donde = document.querySelector(`#${id}`);
  donde.appendChild(que);
}
function createForm(action = "", id = "", method = "") {
  let result;
  let form = document.createElement("form");
  form.setAttribute("action", action);
  form.setAttribute("id", id);
  form.setAttribute("method", method);
  return result = form;
}
function button(value = "", tipo = "", id = "", fun = "") {
  let result;
  let btn = document.createElement("button");
  btn.innerHTML = value;
  btn.setAttribute("type", tipo);
  btn.setAttribute("id", id);
  btn.setAttribute("onclick", fun);
  result = btn;
  return result;
}
function createInputLabel(label, name = "", valor = "") {
  let result = "";
  let caja = document.createElement("div");
  caja.innerHTML = `
    <label>${label}</label>
    `;
  result = caja.innerHTML;
  return "<div>" + result + "</div>";
}
function createInput(id="",name="",data=""){
  let inpt= document.createElement("input");
  inpt.setAttribute("id",id);
  inpt.setAttribute("name",name);
  inpt.setAttribute("data",data);
  return inpt;
}
function createLabel(forIner="",iner=""){
  let label= document.createElement("label");
  label.setAttribute("for",forIner);
  label.innerHTML=iner;
  return label;
}
