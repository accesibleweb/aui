<?php 
$config= "./cursos/config.json";
if (isset($_POST["cuantos"])) {
    $cuantosCursos= $_POST["cuantos"];
        for ($i=0; $i <$cuantosCursos ; $i++) { 
       if(!mkdir("./cursos/curso".$i, 0777, true)) {
           die('Fallo al crear las carpetas...');
       }
    }
    file_put_contents($config,json_encode(array("cursos"=>"")));
    creaJson($config,$cuantosCursos);
}
if (isset($_POST["modulos"],$_POST["formulario"])) {
    $modulos= $_POST["modulos"];
    $formulario=$_POST["formulario"];
        for ($i=0; $i <$modulos; $i++) { 
       if(!mkdir("./cursos/".$formulario."/modulo".$i, 0777, true)) {
           die('Fallo al crear las carpetas...');
       }
    }
    creaModulosJson($config,$modulos,$formulario);
}
if (isset($_POST["curso"], $_POST["modulo"],$_POST["unidad"])) {
        for ($i=0; $i <$_POST["unidad"] ; $i++) { 
       if(!mkdir("./cursos/".$_POST["curso"]."/".$_POST["modulo"]."/unidad".$i, 0777, true)) {
           die('Fallo al crear las carpetas...');
       }
    }
    creaUnidadesJson($config,$_POST["curso"], $_POST["modulo"],$_POST["unidad"]);
}
 
$plantilla='
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
    </head>
    <body>
        <div id="content">
            <header>
                <nav></nav>
            </header>
            <main>
                <section>
                </section>
            </main>
            <footer>
            </footer>
        <div>
    </body>
    </html>
';
if (isset($_POST["cursoPage"], $_POST["moduloPage"],$_POST["unidadPage"],$_POST["paginas"])) {
        for ($i=0; $i <$_POST["paginas"] ; $i++) { 
        $tex="pagina".$i;
       $filePage = fopen("./cursos/".$_POST["cursoPage"]."/".$_POST["moduloPage"]."/".$_POST["unidadPage"]."/"."pagina".$i.".html", "w") or die("Unable to open file!");
        fwrite($filePage, $plantilla);
        fclose($filePage);
    }
    CreaPaginasJson($config,$_POST["cursoPage"], $_POST["moduloPage"],$_POST["unidadPage"],$_POST["paginas"]);
}
 
function creaJson($file,$cantidad){
    $datos = file_get_contents($file);
    $json = json_decode($datos, true);
    $arreglos=array();
     for ($i=0; $i < $cantidad; $i++) {
        array_push($arreglos,array("curso".$i=>"curso contenido".$i));
    }
    $nuevo_contenido=$json["cursos"]=$arreglos;
    $nuevos_datos = json_encode($nuevo_contenido);
    file_put_contents($file, $nuevos_datos); 
}
function creaModulosJson($file,$cantidad,$posicion){
    $datos = file_get_contents($file);
    $json = json_decode($datos, true);
    $arreglos=array();
    for ($i=0; $i <$cantidad ; $i++) { 
        array_push($arreglos,array("modulo".$i=>"modulo contenido".$i));
    }
    $contenido=array();
    foreach ($json as $key => $value) {
      foreach ($value as $keyA => $valueA) {
        $valor=$valueA;
        if ($posicion==$keyA) {
            $valor=$arreglos;
        }
        array_push($contenido,array($keyA => $valor));
     }
    }
     $nuevos_datos = json_encode($contenido);
    file_put_contents($file, $nuevos_datos); 
}
function creaUnidadesJson($file,$curso,$modulo,$unidad){
    $datos = file_get_contents($file);
    $json = json_decode($datos, true);
    $arreglos=array();
    for ($i=0; $i <$unidad ; $i++) { 
        array_push($arreglos,array("unidad".$i=>"unidad contenido".$i));
    }
    $contenido=array();
    foreach ($json as $key => $value) {
        foreach ($value as $keyA => $valueA) {
            $unidades=array();
            foreach ($valueA as $keyb => $valueb) {
                foreach ($valueb as $keyc => $valuec) {
                    $valorc=$valuec;
                    if($keyc==$modulo){
                        $valorc=$arreglos;
                    }
                    array_push($unidades,array($keyc => $valorc));
                }
            }
            array_push($contenido,array($keyA => $unidades));
        }
      }
      $nuevos_datos = json_encode($contenido);
      file_put_contents($file, $nuevos_datos); 
}
function CreaPaginasJson($file,$curso,$modulo,$unidad,$cantidad){
    $datos = file_get_contents($file);
    $json = json_decode($datos, true);
    $arreglos=$cantidad;
    $contenido=array();
    foreach ($json as $key => $value) {
        foreach ($value as $keyA => $valueA) {
            $modulos=array();
            foreach ($valueA as $keyb => $valueb) {
                foreach ($valueb as $keyc => $valuec) {
                    foreach ($valuec as $keyd => $valued) {
                        $unidades=array();
                        foreach ($valued as $keye => $valuee) {
                            $valore=$valuee;
                            if($keye==$unidad){
                                $valore=$cantidad;
                            }
                            array_push($unidades,array($keye=>$valore));
                        }
                    }
                }

            }
            array_push($modulos,array($modulo=>$unidades));
        }
        array_push($contenido,array($keyA => $modulos));
      }
      $nuevos_datos = json_encode($contenido);
      file_put_contents($file, $nuevos_datos);
    
    
}

?>