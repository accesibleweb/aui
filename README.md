Para inciar un repositorio en Windows
 1. crear la carpeta en el escritorio 
 2. Instalar git
 3. Iniciar el CDM en git 
 4. Ir a la carpeta creada para el repositorio
 5. cuando se encuentre en la carpeta ingresar la siguiente línea: 
 	git init
 6. Después clone el repositorio con la siguiente línea: 
 	git clone https://accesibleweb-admin@bitbucket.org/accesibleweb/aui.git
 7. Realice su primer commit con las siguientes líneas:
  	git add aui/ 
	git commit -m "Ingreso al grupo: Usuario"
 8. Ingrese la siguiente línea de código para conectar su origen 
 	git remote add origin https://accesibleweb-admin@bitbucket.org/accesibleweb/aui.git
 9. Pasos de verificación importante realizar 
 	git remote -v
	git fetch (para traer las ramas remotas al origen)

Comandos importantes
 1. git status (saber el estado actual)
 2. git pull origin nombrerama (traer los cambios de una rama)
 3. git push origin nombrerama (subir los cambios a una rama)
 4. git checkout -b nombrerama (crear rama)
 5. git checkout nombrerama (cambiar de rama)
 6. git fetch (para traer las ramas remotas al origen)
 7. git add cambio (para añadir un cambio)
 8. git commit -m "Incidencia trabajada" (realizar un commit)
 9. git branch -D nombrerama (eliminar una rama local)